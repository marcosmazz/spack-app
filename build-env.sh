#!/bin/bash
set -eu

source ./env-settings.sh
mkdir -p ${DEST}
cd ${DEST}
#git clone ${SPACK_REPO} spack || true
#cd spack
#git remote set-url origin ${SPACK_REPO}
#git fetch --all
#git reset --hard origin/${SPACK_TAG}
#cd ..
source spack/share/spack/setup-env.sh


echo Adding compilers to spack
module purge
for comp in gcc nvhpc ; do
    module load $comp
    spack compiler find --scope=site
    module purge
done


echo Building apps
cd build
awk "{ gsub(/DEST_DIR/, \"${DEST}\"); print }" spack.yaml.tmpl > spack.yaml
spack env activate .
spack concretize -f
spack install --fresh -v -y
spack module tcl refresh -y
spack env deactivate
rm -f spack.yaml

